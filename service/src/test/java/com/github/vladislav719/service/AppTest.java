package com.github.vladislav719.service;

import com.github.vladislav719.dto.TeamCreationForm;
import com.github.vladislav719.dto.UserRegistrationForm;
import com.github.vladislav719.model.Photo;
import com.github.vladislav719.model.Team;
import com.github.vladislav719.model.User;
import com.github.vladislav719.model.UserInfo;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

/**
 * Created by vladislav on 06.05.2015.
 */
@ContextConfiguration
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
public class AppTest {

    @Autowired
    UserService userService;

    @Autowired
    TeamService teamService;

    @Autowired
    TournamentService tournamentService;

    @Test
    public void testSaveUser() throws Exception {

        UserRegistrationForm userRegistrationForm = new UserRegistrationForm();
        userRegistrationForm.setPassword("123");
        userRegistrationForm.setConfirmPassword("123");
        userRegistrationForm.setLogin("vlad");

        User user = userService.saveUser(userRegistrationForm);
        Assert.assertNotNull(user);
    }

    @Test
    public void testFindUserById() throws Exception {

        User user = userService.getUserByID(1l);
        Assert.assertNotNull(user);
    }

    @Test
    public void testFindByEmail() throws Exception {

        User user = userService.getUserByEmail("baimurzin.719@gmail.com");
        Assert.assertNotNull(user);
    }

    @Test
    public void testUserInfo() throws Exception {
        UserInfo userInfo = userService.getUserInfo(1l);
        Assert.assertNotNull(userInfo);
    }

    @Test
    public void testUserPhoto() throws Exception {
        Photo photo = userService.getUserPhoto(1l);
        Assert.assertNotNull(photo);
    }

    @Test
    public void testGetTeam() throws Exception{
        Iterable teams = teamService.getAllTeams();
        Assert.assertNotNull(teams);
    }

    @Test
    public void testCreateTeam()  throws Exception {
        TeamCreationForm creationForm = new TeamCreationForm();
        Photo photo = userService.getUserPhoto(1l);
        creationForm.setPhoto(photo);
        creationForm.setTeamName("TestTeam");
        Team team = teamService.createTeam(creationForm);
        Assert.assertNotNull(team);
    }

    @Test
    public void testDeleteTeam() throws Exception {
        boolean deleted = teamService.deleteTeam(1l);
        Assert.assertTrue(deleted);
    }

    @Test
    public void testFindMembers() {
        Iterable members = teamService.getMembers(1l);
        Assert.assertNotNull(members);
    }

    @Test
    public void test() throws Exception {
    }




}
